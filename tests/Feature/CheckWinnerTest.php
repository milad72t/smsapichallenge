<?php

namespace Tests\Feature;

use App\Winner;
use Faker\Factory;
use Illuminate\Support\Str;
use Tests\TestCase;

class CheckWinnerTest extends TestCase
{
    public function test_success_check_winner()
    {
        $winners = Winner::inRandomOrder()->limit(3)->get();

        foreach ($winners as $winner) {
            $response = $this->getJson("/api/v1/codes/$winner->code/winners/$winner->mobile_number");
            $response->assertOk()->assertJson(['status' => true])
                ->assertJsonStructure(['status', 'winner' => ['code', 'mobile_number', 'receive_time']]);
        }
    }

    public function test_failed_check_winner_invalid_data()
    {
        $failedValidationRes = $this->getJson('/api/v1/codes/abcd123456Ac/winners/0933455422');
        $failedValidationRes->assertStatus(404);
    }

    public function test_failed_check_winner_not_found()
    {
        $faker = Factory::create('fa_IR');
        $fakeMobileNumber = $faker->mobileNumber;
        $randomCode = "";

        for ($i = 0; $i < 1000; ++$i) {
            $randomCode = Str::random(10);
            if (Winner::where('code', $randomCode)->where('mobile_number', $fakeMobileNumber)->doesntExist()) {
                break;
            }
        }

        $failedNotFoundRes = $this->getJson("/api/v1/codes/$randomCode/winners/$fakeMobileNumber");
        $failedNotFoundRes->assertStatus(404)->assertJson(['status' => false]);
    }
}
