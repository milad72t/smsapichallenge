<?php

namespace Tests\Feature;

use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Str;
use Tests\TestCase;

class SmsCodeTest extends TestCase
{
    use DatabaseTransactions;

    public function test_success_creation()
    {
        $testData = [
            ['code' => '17', 'limitCount' => 1],
            ['code' => '123aCdE', 'limitCount' => 1000],
            ['code' => Str::random(4), 'limitCount' => 100000],
            ['code' => Str::random(10), 'limitCount' => 200000]
        ];

        foreach ($testData as ['code' => $code, 'limitCount' => $limitCount]) {
            $response = $this->postJson('/api/v1/codes', ['code' => $code, 'limitCount' => $limitCount]);
            $response->assertOk()->assertJson(['status' => true]);
        }
    }

    public function test_failed_creation_duplicate_code()
    {
        $testData = ['code' => 'abC123d', 'limitCount' => 1000];

        $successResponse = $this->postJson('/api/v1/codes', [
            'code' => $testData['code'],
            'limitCount' => $testData['limitCount']
        ]);
        $successResponse->assertOk()->assertJson(['status' => true]);

        $failedDuplicateRes = $this->postJson('/api/v1/codes', [
            'code' => $testData['code'],
            'limitCount' => $testData['limitCount']
        ]);
        $failedDuplicateRes->assertStatus(422)->assertJson(['status' => false])
            ->assertJsonStructure(['status', 'errors' => ['code']]);;
    }

    public function test_failed_creation_invalid_data()
    {
        $failedEmptyDataRes = $this->postJson('/api/v1/codes');
        $failedEmptyDataRes->assertStatus(422)->assertJson(['status' => false])
            ->assertJsonStructure(['status', 'errors' => ['code', 'limitCount']]);;

        $failedStringLimitRes = $this->postJson('/api/v1/codes', [
            'limitCount' => Str::random(5),
        ]);
        $failedStringLimitRes->assertStatus(422)->assertJson(['status' => false])
            ->assertJsonStructure(['status', 'errors' => ['code']]);

        $failedBigCodeRes = $this->postJson('/api/v1/codes', [
            'code' => Str::random(random_int(11, 20)),
            'limitCount' => random_int(2, 10000),
        ]);
        $failedBigCodeRes->assertStatus(422)->assertJson(['status' => false])
            ->assertJsonStructure(['status', 'errors' => ['code']]);
    }
}
