<?php

use Illuminate\Support\Facades\Route;

Route::get('/', function () {
    return view('welcome');
});

Route::get('/test', function () {
    dd(\Illuminate\Support\Facades\Redis::flushdb());
    dd((new \App\Repositories\CodeRepository())->limitCount('chicken123'));
});
