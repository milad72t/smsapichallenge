<?php

namespace App\Jobs;

use App\ReceivedCode;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;
use Illuminate\Support\Facades\Validator;

class InsertReceivedCodesToDB implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private $mobileNumber;
    private $code;
    private $receivedTime;

    public function __construct($mobileNumber, $code, $receivedTime)
    {
        $this->mobileNumber = $mobileNumber;
        $this->code = $code;
        $this->receivedTime = $receivedTime;
    }

    public function handle()
    {
        $data = [
            'mobileNumber' => $this->mobileNumber,
            'code' => $this->code,
            'receivedTime' => $this->receivedTime
        ];

        $validation = [
            'mobileNumber' => 'required | size:11',
            'code' => 'required | string | min:1 | max:10', //TODO: custom code validation
            'receivedTime' => 'required | date'
        ];

        $validator = Validator::make($data, $validation);

        if (!$validator->passes()) {
            //TODO: Log invalid data
            return;
        }

        ReceivedCode::create($data);
    }
}
