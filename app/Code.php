<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Code extends Model
{
    protected $guarded = [];

    public function winners()
    {
        return $this->hasMany(Winner::class, 'code', 'code');
    }
}
