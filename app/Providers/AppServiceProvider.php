<?php

namespace App\Providers;

use App\Classes\BackGroundReceivedCodesProcessor;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{

    public function register()
    {
        $this->app->bind('BackGroundCodeProcessor', function () {
            return new BackGroundReceivedCodesProcessor();
        });
    }

    public function boot()
    {
        //
    }
}
