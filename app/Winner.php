<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Winner extends Model
{
    protected $table = 'code_winners';
    protected $guarded = [];
    public $incrementing = false;
    public $timestamps = false;

    public function code()
    {
        return $this->belongsTo(Code::class, 'code', 'code');
    }
}
