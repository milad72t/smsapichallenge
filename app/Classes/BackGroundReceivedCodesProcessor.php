<?php

namespace App\Classes;

use App\ReceivedCode;
use App\Repositories\WinnerRepository;
use App\Code;
use App\Winner;

class BackGroundReceivedCodesProcessor // back ground process version - obsoleted
{
    private const CHUNK_SIZE = 100;
    private $smsCodes;
    private $winnersRepository;

    public function __construct()
    {
        $this->smsCodes = Code::withCount('winners')->get();
        $this->winnersRepository = new WinnerRepository();
    }

    public function process()
    {
        ReceivedCode::orderBy('receivedTime', 'asc')->chunk(self::CHUNK_SIZE, function ($items) {
            $winners = [];
            foreach ($items as $item) {
                if (!$this->codeHasBlankSpace($item->code)) {
                    continue;
                }
                if ($this->isUserInWinnerArray($winners, $item->mobileNumber, $item->code)) {
                    continue;
                }
                if ($this->hadUserWinThisCode($item->mobileNumber, $item->code)) {
                    continue;
                }
                array_push($winners, $this->winnerArray($item->mobileNumber, $item->code, $item->receivedTime));
                $this->incrementWinnerCounts($item->code, 1);
            }

            $insertStatus = $this->insertWinnersToDB($winners);
            if ($insertStatus) {
                $itemsIdToDelete = $items->pluck('_id')->all();
                $this->removeReceivedCodes($itemsIdToDelete);
            }
        });
    }

    private function codeHasBlankSpace(string $code): bool
    {
        return $this->smsCodes->where('code', $code)->filter(function ($item) {
            return $item->limit_count > $item->winners_count;
        })->count();
    }

    private function isUserInWinnerArray(array $winnersArray, string $mobileNumber, string $code)
    {
        foreach ($winnersArray as ['code' => $winnerCode, 'mobile_number' => $winnerMobileNumber]) {
            if ($winnerCode == $code && $winnerMobileNumber == $mobileNumber) {
                return true;
            }
        }
        return false;
    }

    private function hadUserWinThisCode(string $mobileNumber, string $code): bool
    {
        return Winner::where('mobile_number', $mobileNumber)->where('code', $code)->exists();
    }

    private function winnerArray(string $mobileNumber, string $code, $receiveTime): array
    {
        return [
            'mobile_number' => $mobileNumber,
            'code' => $code,
            'receive_time' => $receiveTime
        ];
    }

    private function insertWinnersToDB(array $winnersArray): bool
    {
        try {
            $this->winnersRepository->bulkyInsert($winnersArray);
            return true;
        } catch (\Exception $exp) {
            $countOfEachCode = array_count_values(array_column($winnersArray, 'code'));
            foreach ($countOfEachCode as $code => $count) {
                $this->decrementWinnerCount($code, $count);
            }
            //TODO: Log Error and insert them one by one to detect the failure
            return false;
        }
    }

    private function removeReceivedCodes(array $itemsId)
    {
        ReceivedCode::destroy($itemsId);
    }

    private function incrementWinnerCounts(string $code, int $count)
    {
        $this->smsCodes->where('code', $code)->each(function ($item) use ($count) {
            $item->winners_count += $count;
        });
    }

    private function decrementWinnerCount(string $code, int $count)
    {
        $this->smsCodes->where('code', $code)->each(function ($item) use ($count) {
            $item->winners_count -= $count;
        });
    }
}