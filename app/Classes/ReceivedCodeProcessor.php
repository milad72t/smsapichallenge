<?php

namespace App\Classes;

use App\Repositories\CodeRepository;
use App\Repositories\WinnerRepository;
use Illuminate\Support\Facades\Redis;

class ReceivedCodeProcessor
{
    private $code;
    private $mobileNumber;
    private $receivedDate;
    private $winnerRepository;
    private $codeRepository;

    public function __construct(string $code, string $mobileNumber, string $receivedDate)
    {
        $this->code = $code;
        $this->mobileNumber = $mobileNumber;
        $this->receivedDate = $receivedDate;
        $this->codeRepository = new CodeRepository();
        $this->winnerRepository = new WinnerRepository();
    }

    public function process(): bool
    {
        if (!$this->codeHasFreeSpace()) {
            return false;
        }
        $addToSetResult = Redis::sadd($this->code, $this->mobileNumber);
        if (!$addToSetResult) {
            return false;
        }
        $status = $this->setRequestAsWinner();
        return $status;
    }

    private function codeHasFreeSpace(): bool
    {
        $codeRemainingCount = $this->codeRepository->remainingCount($this->code);
        return $codeRemainingCount > 0;
    }

    private function setRequestAsWinner(): bool
    {
        $insertToDbStatus = $this->insertToDB();
        if (!$insertToDbStatus) {
            Redis::srem($this->code, $this->mobileNumber);
        }
        return $insertToDbStatus;
    }

    private function insertToDB(): bool
    {
        try {
            $this->winnerRepository->create($this->code, $this->mobileNumber, $this->receivedDate);
            return true;
        } catch (\Exception $exception) {
            //TODO: Log The Error
            return false;
        }
    }
}