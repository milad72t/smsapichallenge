<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SendCodeRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'mobileNumber' => ['required', 'string', 'regex:/^(09|\+989)\d{9}$/i'],
            'code' => ['required', 'string', 'min:1', 'max:10'],
        ];
    }
}
