<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateSmsCode extends FormRequest
{

    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        return [
            'code' => 'required | string | min:2 | max:10 | unique:codes',
            'limitCount' => 'required | integer'
        ];
    }
}
