<?php

namespace App\Http\Controllers;

use App\Classes\ReceivedCodeProcessor;
use App\Http\Requests\SendCodeRequest;
use App\Jobs\InsertReceivedCodesToDB;
use Carbon\Carbon;
use Illuminate\Http\Request;

class CodeReceiverController extends Controller
{

    private const FAILED_MGS = "Sorry, you didn't win :(";
    private const SUCCESS_MGS = "hooray!, you won ;)";

    public function checkCode(SendCodeRequest $request)
    {
        $code = $request->get('code');
        $mobileNumber = str_replace('+98', '0', $request->get('mobileNumber'));
        $receivedTime = Carbon::now()->toDateTimeString('millisecond');

        $codeProcessor = new ReceivedCodeProcessor($code, $mobileNumber, $receivedTime);
        $status = $codeProcessor->process();

        return response()->json([
            'status' => $status,
            'msg' => $status ? self::SUCCESS_MGS : self::FAILED_MGS
        ]);
    }


    public function create(Request $request)
    {
        $mobileNumber = $request->get('mobileNumber');
        $code = $request->get('code');
        $receivedTime = Carbon::now()->toDateTimeString('millisecond');

        InsertReceivedCodesToDB::dispatch($mobileNumber, $code, $receivedTime);

        return response()->json([
            'status' => true,
            'msg' => 'Your Message Received Successfully'
        ]);
    }
}
