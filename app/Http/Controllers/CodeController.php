<?php

namespace App\Http\Controllers;

use App\Http\Requests\CreateSmsCode;
use App\Repositories\CodeRepository;

class CodeController extends Controller
{
    private $codeRepository;

    public function __construct(CodeRepository $codeRepository)
    {
        $this->codeRepository = $codeRepository;
    }

    public function create(CreateSmsCode $request)
    {
        $code = $request->get('code');
        $limitCount = $request->get('limitCount');

        $this->codeRepository->create($code, $limitCount);

        return response()->json([
            'status' => true,
            'msg' => 'Your Code Saved Successfully'
        ]);
    }
}
