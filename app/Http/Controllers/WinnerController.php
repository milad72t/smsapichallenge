<?php

namespace App\Http\Controllers;

use App\Repositories\WinnerRepository;

class WinnerController extends Controller
{
    private $winnerRepository;

    public function __construct(WinnerRepository $winnerRepository)
    {
        $this->winnerRepository = $winnerRepository;
    }

    public function get($smsCode, $mobileNumber)
    {
        $winner = $this->winnerRepository->find($smsCode, $mobileNumber);

        return response()->json([
            'status' => true,
            'winner' => $winner
        ]);
    }
}
