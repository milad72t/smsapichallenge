<?php

namespace App;

use Jenssegers\Mongodb\Eloquent\Model;

class ReceivedCode extends Model
{
    protected $connection = 'mongodb';
    protected $guarded = [];
    public $timestamps = false;
}
