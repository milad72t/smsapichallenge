<?php

namespace App\Repositories;

use App\Winner;
use Illuminate\Support\Facades\Cache;

class WinnerRepository
{

    public function find(string $code, string $mobileNumber): Winner
    {
        return Winner::where('code', $code)->where('mobile_number', $mobileNumber)->firstOrFail();
    }

    public function create(string $code, string $mobileNumber, $receiveTime): Winner
    {
        $winner = Winner::create([
            'mobile_number' => $mobileNumber,
            'code' => $code,
            'receive_time' => $receiveTime
        ]);
        $remainingCountCacheKey = 'code_remaining_count_' . $code;
        if ($remainingCountCacheKey) {
            Cache::decrement('code_remaining_count_' . $code);
        }

        return $winner;
    }

    public function bulkyInsert(array $data)
    {
        Winner::insert($data);
    }

}