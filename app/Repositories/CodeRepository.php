<?php

namespace App\Repositories;

use App\Code;
use Illuminate\Support\Facades\Cache;

class CodeRepository
{
    public function create(string $code, int $limitCount): Code
    {
        return Code::create([
            'code' => $code,
            'limit_count' => $limitCount
        ]);
    }

    public function remainingCount(string $code): int
    {
        return Cache::rememberForever('code_remaining_count_' . $code,
            function () use ($code) {
                $codeObject = Code::withCount('winners')->where('code', $code)->first();
                if (!$codeObject) {
                    return 0;
                }
                return $codeObject->limit_count - $codeObject->winners_count;
            });
    }

    public function limitCount(string $code): int
    {
        return Cache::rememberForever('code_limit_count_' . $code,
            function () use ($code) {
                $codeObject = Code::where('code', $code)->first();
                if (!$codeObject) {
                    return 0;
                }
                return $codeObject->limit_count;
            });
    }
}